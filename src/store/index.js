import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    api_route:{
      baseUrl: 'http://84.252.73.207/api',
      login: '/login',
      register: '/register',
      repair: '/repair',
      comments: '/comments'
    }
  },
  getters: {
    getRoute: state => state.api_route
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
